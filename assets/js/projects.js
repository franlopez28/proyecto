var jsonr;
var fecha = new Date();
var year = fecha.getFullYear();
var datos = [];

var width = $(window).width();
$(window).on('resize', function(){
	if($(this).width() != width){
		width = $(this).width();
		drawBasic();
	}
});

$(document).ready(function()
{
	loadUserData();
	document.getElementById("defaultOpen").click();

	for (var i = year; i >= 2016; i--) {
		$("#filteryear").append("<option>"+i+"</option>");
		$("#filteryearGrafica").append("<option>"+i+"</option>");
	}
});

// document.addEventListener('DOMContentLoaded', function() {
// 	loadContratos();
// });

function relocate(page,params)
{
	var body = document.body;
	form=document.createElement('form');
	form.method = 'POST';
	form.action = page;
	form.name = 'jsform';

	for (index in params)
	{
		var input = document.createElement('input');
		input.type='hidden';
		input.name=index;
		input.id=index;
		input.value=params[index];
		form.appendChild(input);
	}

	body.appendChild(form);
	form.submit();
}

function saveUserData()
{
	//var parametros = 'd_firstName='+ $('#nombre').val() + '&d_lastName=' + $('#apellido').val()+'&d_email='+ $('#email').val()+'&d_invoiceAddress='+ $('#address').val()+'&d_invoicePc='+ $('#pc').val()+'&d_invoiceProvince='+$('#province').val()+'&d_invoiceTown='+ $('#town').val()+'&d_invoiceCountry='+ $('#country').val()+'&d_nameContrato='+ $('#contrato').val()+'&d_tipoContrato='+ $('#tipo').val()+'&d_priceContrato='+ $('#precio').val();
	var parametros = 'fn='+ $('#nombre').val()+
					'&ln=' + $('#apellido').val()+
					'&em='+ $('#email').val()+
					'&iad='+ $('#address').val()+
					'&ipc='+ $('#pc').val()+
					'&ipr='+$('#province').val()+
					'&ito='+ $('#town').val()+
					'&ico='+ $('#country').val();

	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/saveUserData.php",
		data: parametros,
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {

			if (response.success=="1") {
				console.log(response);
			}
		}
	});   
}
/*
function updateContratoData()
{
	//var parametros = '_fk_maestrodecontratos='+ $('#contratos').val();
	var parametros = 'fkmc='+ $('#contratos').val();

	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/updateContratoData.php",
		data: parametros,
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {
			if (response.success=="1") {
				console.log(response);
				$("#contrato").val($("#contratoM").val());
				$("#tipo").val($("#tipoM").val());
				$("#precio").val($("#precioM").val());
			}
		}
	});  
}
*/
function validatePass()
{
	var pass = $('#contrasena').val();
	var passnueva = $('#contrasenanueva').val();
	var confirmar = $('#contrasenaconfirmar').val();

	if (passnueva!="" && confirmar!="" && passnueva!== pass && passnueva === confirmar) {
		$('#btnChPass').prop('disabled', false);
		$('#contrasenanueva').css("border-color","green");
		$('#contrasenaconfirmar').css("border-color","green");
	} else {
		if (passnueva=="" && confirmar==""){
			$('#btnChPass').prop('disabled', true);
			$('#contrasenanueva').css("border-color","#ccc");
			$('#contrasenaconfirmar').css("border-color","#ccc");
		} else {
			$('#btnChPass').prop('disabled', true);
			$('#contrasenanueva').css("border-color","red");
			$('#contrasenaconfirmar').css("border-color","red");
		}
	}
}

function changepassword()
{
	var pass = $('#contrasena').val();
	var passnueva = $('#contrasenanueva').val();
	var confirmar = $('#contrasenaconfirmar').val();
	var parametros = 'pw='+ $('#contrasenanueva').val();

	if (passnueva!="" && confirmar!="" && passnueva!== pass && passnueva === confirmar) {
		$.ajax({
			method: "POST",
			url: "assets/apis/fmapi/Services/changepassword.php",
			data: parametros,
			dataType: "json",
			success: function (response) {
				if (response.success=="1") {
					console.log(response);
					$('#contrasena').val(passnueva);
					$('#contrasenanueva').val("");
					$('#contrasenaconfirmar').val("");
					$('#btnChPass').prop('disabled', true);
					$('#contrasenanueva').css("border-color","#ccc");
					$('#contrasenaconfirmar').css("border-color","#ccc");
				}
			}
		});
	}
}

function getContrato()
{

	var parametros = 'pk='+ $('#contratos').val();

	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/getContractData.php",
		data: parametros,
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {

			if (response.success=="1") {
				console.log(response);
				$("#contratoM").val(response.data.d_firstName);
				$("#tipoM").val(response.data.d_tipo);
				$("#precioM").val(response.data.d_precio);
			}
		}
	});
}

function loadUserData()
{
	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/loadUserData.php",
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {

			if (response.success=="1") {
				$("#nombre").val(response.data[0].d_firstName);
				$("#apellido").val(response.data[0].d_lastName);
				$("#email").val(response.data[0].d_email);
				$("#address").val(response.data[0].d_invoiceAddress);
				$("#pc").val(response.data[0].d_invoicePc);
				$("#province").val(response.data[0].d_invoiceProvince);
				$("#town").val(response.data[0].d_invoiceTown);
				$("#country").val(response.data[0].d_invoiceCountry);
				$("#contrasena").val(response.data[0].d_password);
				$("#contrato").val(response.data[0].d_nameContrato);
				$("#tipo").val(response.data[0].d_tipoContrato);
				$("#precio").val(response.data[0].d_priceContrato);
				$("#contratoM").val(response.data[0].d_nameContrato);
				$("#tipoM").val(response.data[0].d_tipoContrato);
				$("#precioM").val(response.data[0].d_priceContrato);

				for (var i = 0; i < response.data[0].l_contratos.length; i++) {
					$("#contratos").append("<option value='"+response.data[0].l_contratos[i][0]+"'>"+response.data[0].l_contratos[i][1]+"</option>");
				}

				$("#contratos").val(response.data[0]._fk_maestrodecontratos);

			}
		}
	});
}

function loadContratos(anyo)
{
	year = anyo;
	$("#filteryear").val(year);
	$("#filteryearGrafica").val(year);
	var parametros = "year="+year;
	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/loadContratos.php",
		data: parametros,
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {
			if (response.success=="1") {
				clearDraw();
				
				$("#contratoslist").html("");
				for (var i = 0; i < response.data.length; i++) {
					var lastrow="";
					var monthnum = parseInt(response.data[i].l_mes);
					
					var amount = response.data[i].l_TotalPlusVAT;
					var item = parseInt(amount.replace(",","."));
					datos[monthnum - 1][1]=item;

					if (response.data[i].l_empty=="1") {
						lastrow="<td><button onclick='viewPDF(\""+response.data[i].__pk+"\")'><i class='fa fa-eye' style='font-weight: bold;font-size: 14px;'></i></button> <button style='margin-left:0.5em;' onclick='downloadPDF(\""+response.data[i].__pk+"\")'><i class='fa fa-download' style='font-weight: bold;font-size: 14px;'></i></button></td>";
					} else {
						lastrow="<td></td>";
					}
					$("#contratoslist").append(
						"<tr>"+
						"<td>"+response.data[i].d_mes+"</td>"+
						"<td>"+response.data[i].d_date+"</td>"+
						"<td>"+response.data[i].l_TotalPlusVAT+" €</td>"+
						lastrow
						+"</tr>");
				}

				drawBasic();
			}else{
				clearDraw();
				drawBasic();
			}
		}
	});
}
function clearDraw(){

datos[0]=["Enero",null] ;
datos[1]=["Febrero",null] ;
datos[2]=["Marzo",null] ;
datos[3]=["Abril",null] ;
datos[4]=["Mayo",null] ;
datos[5]=["Junio",null] ;
datos[6]=["Julio",null] ;
datos[7]=["Agosto",null] ;
datos[8]=["Septiembre",null] ;
datos[9]=["Octubre",null] ;
datos[10]=["Noviembre",null] ;
datos[11]=["Diciembre",null] ;
}

function viewPDF(id)
{
	console.log("Viewing pdf of "+id);

	var parametros = "pk="+id;

	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/getContractPDF.php",
		data: parametros,
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {
			console.log(response);
			$("body").append("<form id='tempform' action='assets/apis/fmapi/Services/pdf.php' target='_blank' method='post'>"+
							"<input name='action' value='view'/>"+
							"<input name='name' value='"+response.data.name+"'/>"+
							"<input name='b64' value='"+response.data.b64+"'/>"+
							"</form>");
			$("#tempform").submit();
			$("#tempform").remove();
		}
	});
}

function downloadPDF(id)
{
	console.log("Downloading pdf of "+id);
	var parametros = "pk="+id;

	$.ajax({
		method: "POST",
		url: "assets/apis/fmapi/Services/getContractPDF.php",
		data: parametros,
		dataType: "json",
		beforeSend: function (){
		},
		success: function (response) {
			console.log(response);
			$("body").append("<form id='tempform' action='assets/apis/fmapi/Services/pdf.php' target='_blank' method='post'>"+
							"<input name='action' value='download'/>"+
							"<input name='name' value='"+response.data.name+"'/>"+
							"<input name='b64' value='"+response.data.b64+"'/>"+
							"</form>");
			$("#tempform").submit();
			$("#tempform").remove();
		}
	});
}

function openTabs(evt, id)
{

	if (id!="Datos") {
		loadContratos(year);
	}

	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(id).style.display = "block";
	evt.currentTarget.className += " active";
}

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Meses');
      data.addColumn('number', 'Consumo');
      data.addRows(datos);

     var options = {
       title: 'AÑO '+year,
       width: $(window).width()*0.90,
       height: $(window).height()*0.90,
       colors: ['#4caf50'],
       legend: 'none',
       bar: {groupWidth: '95%'},
       chartArea: {
        backgroundColor: {
          fill: '#ccc',
          fillOpacity: 0.3
        },
         
      },
      backgroundColor: {
        fill: '#fff',
        fillOpacity: 0.1
      },
       vAxis: { gridlines: { count: 4 } }
     };

      var chart = new google.visualization.BarChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }