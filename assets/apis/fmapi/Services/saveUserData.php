<?php

require('getConnection.php');
require('../Classes/Customer.php');

session_start();

if ($_POST != "") {
	$con = unserialize($_SESSION["con"]);

	$customer = new Customer($_SESSION['pk'],null,$_POST['fn'],$_POST['ln'],$_POST['em'],null,$_POST['iad'],$_POST['ipc'],$_POST['ito'],$_POST['ipr'],$_POST['ico'],null);

	$result = $customer->update($con);

	if (!$result) {
		echo json_encode(array(
			"success"=>"0",
			"message"=>"Error message"
		));
	} else {
		echo json_encode(array(
			"success"=>"1",
			"message"=>"Success message"
		));
	}

} else {
	echo json_encode(array(
			"success"=>"0",
			"message"=>"Error message"
		));
}

?>