<?php


require('getConnection.php');
require('../Classes/Contract.php');


$con = unserialize($_SESSION["con"]);

$year = $_POST["year"];

$contract = new Contract(null,null,null,$year,null,$_SESSION['pk'],null,null);

$result = $contract->select($con);

if (!$result) {
	echo json_encode(array(
		"success"=>"0",
		"message"=>"Error message"
	));
} else {
	echo json_encode(array(
		"success"=>"1",
		"data"=>$result
	));
}

?>