<?php

require('getConnection.php');
require('../Classes/Master.php');

$con = unserialize($_SESSION["con"]);

$master = new Master($_POST['pk'],null,null,null);

$result = $master->select($con);

if (!$result) {
	echo json_encode(array(
		"success"=>"0",
		"message"=>"Error message"
	));
} else {
	echo json_encode(array(
		"success"=>"1",
		"data"=>$result
	));
}

?>