<?php

require('getConnection.php');
require('../Classes/Customer.php');


session_start();

if ($_POST['fkmc'] != "") {
	$con = unserialize($_SESSION["con"]);

	$customer = new Customer($_SESSION['pk'],$_POST['fkmc'],null,null,null,null,null,null,null,null,null,null,null,null,null);

	$result = $customer->update($con);

	if (!$result) {
		echo json_encode(array(
			"success"=>"0",
			"message"=>"Error message"
		));
	} else {
		echo json_encode(array(
			"success"=>"1",
			"message"=>"Success message"
		));
	}

} else {
	echo json_encode(array(
			"success"=>"0",
			"message"=>"Error message"
		));
}

?>