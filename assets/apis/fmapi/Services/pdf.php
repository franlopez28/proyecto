<?php

$action = $_POST['action'];
$name = $_POST['name'];
$filename = "../../../temp/".$_POST['name'];
$b64 = $_POST['b64'];

if (!empty($action)&&!empty($name)&&!empty($b64)) {
	file_put_contents($filename, base64_decode($b64));

	switch ($action) {
		case 'view':
			header("Content-Type: application/pdf");
			header("Content-Disposition: inline; filename='".$name."'");
			break;
		case 'download':
			header('Content-Description: File Transfer');
    		header('Content-Type: application/octet-stream');
    		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate');
   			header('Pragma: public');
    		header('Content-Length: ' . filesize($filename));
    		
			break;
	}
	readfile($filename);
}


?>