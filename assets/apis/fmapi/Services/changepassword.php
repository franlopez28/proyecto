<?php

require('getConnection.php');
require_once('../Classes/Customer.php');

$con = unserialize($_SESSION["con"]);

$customer = new Customer($_SESSION['pk'],null,null,null,null,$_POST['pw'],null,null,null,null,null,null,null,null,null);

$result = $customer->update($con);

if (!$result) {
	echo json_encode(array(
		"success"=>"0",
		"message"=>"Error message",
		"customer"=>$customer
	));
} else {
	echo json_encode(array(
		"success"=>"1",
		"message"=>"Success message"
	));
}

?>