<?php

require('getConnection.php');
require('../Classes/Contract.php');

$con = unserialize($_SESSION["con"]);

$contract = new Contract($_REQUEST['pk'],null,null,null,null,null,null);

$result = $contract->getb64($con);

if (!$result) {
	echo json_encode(array(
		"success"=>"0",
		"message"=>"Error message"
	));
} else {
	$action = $_REQUEST['action'];
	$name = $result['name'];
	$filename = "../../../temp/".$result['name'];
	$b64 = $result['b64'];

	if (!empty($action)&&!empty($name)&&!empty($b64)) {
		file_put_contents($filename, base64_decode($b64));
		header("Content-Type: application/pdf");
		switch ($action) {
			case 'view':
				header("Content-Disposition: inline; filename='".$name."'");
				readfile($filename);
				break;
			case 'download':
				header("Content-Disposition: attachment; filename='".$name."'");
				exit;
				break;
		}
		
	}
}

?>