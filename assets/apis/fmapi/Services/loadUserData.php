<?php

require('getConnection.php');
require_once('../Classes/Customer.php');

session_start();

$con = unserialize($_SESSION["con"]);

$customer = new Customer($_SESSION['pk'],null,null,null,null,null,null,null,null,null,null,null);

$result = $customer->select($con);

if (!$result) {
	echo json_encode(array(
		"success"=>"0",
		"message"=>"Error message"
	));
} else {
	echo json_encode(array(
		"success"=>"1",
		"data"=>$result
	));
}

?>