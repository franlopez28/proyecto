<?php

require('getConnection.php');
require('../Classes/Contract.php');

$con = unserialize($_SESSION["con"]);

$contract = new Contract($_POST['pk'],null,null,null,null,null,null);

$result = $contract->getb64($con);

if (!$result) {
	echo json_encode(array(
		"success"=>"0",
		"message"=>"Error message"
	));
} else {
	echo json_encode(array(
		"success"=>"1",
		"data"=>$result
	));
}

?>