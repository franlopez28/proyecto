<?php

require('getConnection.php');
require_once('../Classes/Customer.php');

if (!empty($_POST['email'])&&!empty($_POST['passwd'])) {
	$con = unserialize($_SESSION["con"]);

	$email=str_replace("@", ":sep:", $_POST['email']);

	$cust = new Customer(null,null,null,null,$email,$_POST['passwd'],null,null,null,null,null,null,null,null,null);

	$result = $cust->login($con);
	
	if (!$result) {

		echo json_encode(array(
			"success"=>"0",
			"message"=>"Usuario o contraseña incorrectos"
		));
	
	} else {
		$_SESSION['idRecord'] = $result["id"];
		$_SESSION['pk'] = $result["pk"];
		$_SESSION['loginuser'] = True;
		echo json_encode(array(
			"success"=>"1",
			"message"=>"Login correcto"
		));
	}
}else{
	echo json_encode(array(
			"success"=>"0",
			"message"=>"Por favor, rellena todos los campos"
		));
}

?>
