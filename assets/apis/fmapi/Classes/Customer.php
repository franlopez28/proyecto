<?php

require_once('FileMaker.php');

class Customer
{
    //Atributos
    public $__pk,$_fk_maestrodecontratos,$d_firstName,$d_lastName,$d_email,$d_password,$d_invoiceAddress,$d_invoicePc,$d_invoiceTown,$d_invoiceProvince,$d_invoiceCountry;
    private $layout = "W - Customers Web / [cuwe1]";

    //Constructor
    public function __construct($__pk=null,
                                $_fk_maestrodecontratos=null,
                                $d_firstName=null,
                                $d_lastName=null,
                                $d_email=null,
                                $d_password=null,
                                $d_invoiceAddress=null,
                                $d_invoicePc=null,
                                $d_invoiceTown=null,
                                $d_invoiceProvince=null,
                                $d_invoiceCountry=null)
    {
        $this->__pk=$__pk;
        $this->_fk_maestrodecontratos=$_fk_maestrodecontratos;
        $this->d_firstName=$d_firstName;
        $this->d_lastName=$d_lastName;
        $this->d_email=$d_email;
        $this->d_password=$d_password;
        $this->d_invoiceAddress=$d_invoiceAddress;
        $this->d_invoicePc=$d_invoicePc;
        $this->d_invoiceTown=$d_invoiceTown;
        $this->d_invoiceProvince=$d_invoiceProvince;
        $this->d_invoiceCountry=$d_invoiceCountry;
    }

    /**
     * Con la variable $con llama a la funcion select de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function select(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
        	$keys[$cont] = $key;
        	$vals[$cont] = $fields[$key];
        	$cont++;
        }
        $result = $con->gotolayoutMultiFilter($this->layout,$keys,$vals);
        if (!$result) {
        	return $result;
        } else {
        	$customers = array();
        	foreach ($result as $rec) {
        		foreach ($fields as $key => $value) {
        			$cust[$key] = $rec->getField($key); 
        		}

        		$customer = new Customer($cust["__pk"],$cust["_fk_maestrodecontratos"],$cust["d_firstName"],$cust["d_lastName"],$cust["d_email"],$cust["d_password"],$cust["d_invoiceAddress"],$cust["d_invoicePc"],$cust["d_invoiceTown"],$cust["d_invoiceProvince"],$cust["d_invoiceCountry"]);
        		array_push($customers, $customer);
        	}
        	return $customers;
        }
    }

    /**
     * Con la variable $con llama a la funcion insert de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return boolean
     */
    function insert(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
        	$keys[$cont] = $key;
        	$values[$cont] = $values;
        	$cont++;
        }
        $result = $con->createNewRecord($this->layout,$parametros);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Con la variable $con llama a la funcion update de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return boolean
     */
    function update(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        unset($parametros["__pk"]);
        unset($parametros["layout"]);
        $result = $con->SetDatas($this->layout,"__pk",$this->__pk,$parametros);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Con la variable $con llama a la funcion delete de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return boolean
     */
    function delete(Connection $con){
        $parametros=get_object_vars($this);
        $result = $con->deleteMultiFilter($this->layout,"__pk",$parametros["__pk"]);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    function login(Connection $con)
    {
        $fields[0] = "l_email";
        $values[0] = $this->d_email;
        $result = $con->gotolayoutMultiFilter($this->layout, $fields, $values);
        if (!$result) {
            return 0;
        } else {
            $data['id'] = $result[0]->getRecordId();
            $data['pk'] = $result[0]->getField("__pk");
            $data['pass'] = $result[0]->getField("d_password");
            if ($this->d_password===$data["pass"]) {
                return $data;
            } else {
                return 0;
            }
        }
    }
}