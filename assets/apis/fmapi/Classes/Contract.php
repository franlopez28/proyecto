<?php

require_once('FileMaker.php');

class Contract
{
    //Atributos
    public $__pk,$d_date,$d_mes,$d_year,$l_TotalPlusVAT,$_fk_customer,$l_empty,$l_mes;
    private $layout = "W - Contracts Web / [cont100]";

    //Constructor
    public function __construct($__pk=null,
                                $d_date=null,
                                $d_mes=null,
                                $d_year=null,
                                $l_TotalPlusVAT=null,
                                $_fk_customer=null,
                                $l_empty=null,
                                $l_mes=null)
    {
        $this->__pk=$__pk;
        if ($d_date==null) {
            $this->d_date=$d_date;
        }else{
            $date_en = str_replace("/", "", $d_date);
            $day=substr($date_en, 2,2);
            $month=substr($date_en, 0,2);
            $year=substr($date_en, 4,4);
            $this->d_date=$day."/".$month."/".$year;
        }
        $this->d_mes=$d_mes;
        $this->d_year=$d_year;
        $this->l_TotalPlusVAT=$l_TotalPlusVAT;
        $this->_fk_customer=$_fk_customer;
        $this->l_empty=$l_empty;
        $this->l_mes=$l_mes;

    }

    /**
     * Con la variable $con llama a la funcion select de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function select(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
            $keys[$cont] = $key;
            $vals[$cont] = $fields[$key];
            $cont++;
        }
        $result = $con->gotolayoutMultiFilter($this->layout,$keys,$vals);
        if (!$result) {
        	return false;
        } else {
        	$contracts = array();
        	foreach ($result as $rec) {
        		foreach ($fields as $key => $value) {
        			$cotr[$key] = $rec->getField($key); 
        		}
        		$contract = new Contract($cotr["__pk"],$cotr["d_date"],$cotr["d_mes"],$cotr["d_year"],$cotr["l_TotalPlusVAT"],$cotr["_fk_customer"],$cotr["l_empty"],$cotr["l_mes"]);
        		array_push($contracts, $contract);
        	}
        	return $contracts;
        }
    }

    /**
     * Con la variable $con llama a la funcion insert de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function insert(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
        	$keys[$cont] = $key;
        	$values[$cont] = $values;
        	$cont++;
        }
        $result = $con->createNewRecord($this->layout,$parametros);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Con la variable $con llama a la funcion update de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function update(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
        	$keys[$cont] = $key;
        	$values[$cont] = $values;
        	$cont++;
        }
        unset($parametros["__pk"]);
        $result = $con->SetDatas($this->layout,"__pk",$fields["__pk"],$parametros);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Con la variable $con llama a la funcion delete de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @param array
     */
    function delete(Connection $con){
        $parametros=get_object_vars($this);
        $result = $con->deleteMultiFilter($this->layout,"__pk",$parametros["__pk"]);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    function getb64(Connection $con)
    {
        $fields[0] = "__pk";
        $values[0] = $this->__pk;
        $result = $con->gotolayoutMultiFilter($this->layout, $fields, $values);
        if (!$result) {
            return 0;
        } else {
            $data['name'] = $result[0]->getField("l_container");
            $data['b64'] = $result[0]->getField("l_base64");
            return $data;
        }
    }
}