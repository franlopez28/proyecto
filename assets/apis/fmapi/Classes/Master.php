<?php

require_once('FileMaker.php');

class Master
{
    //Atributos
    public $__pk,$d_firstName, $d_tipo,$d_precio;
    private $layout = "W - Maestro de contratos Web / [madw1]";

    //Constructor
    public function __construct($__pk=null,
                                $d_firstName=null,
                                $d_tipo=null,
                                $d_precio=null)
    {
        $this->__pk=$__pk;
        $this->d_firstName=$d_firstName;
        $this->d_tipo=$d_tipo;
        $this->d_precio=$d_precio;
    }

    /**
     * Con la variable $con llama a la funcion select de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function select(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
            $keys[$cont] = $key;
            $vals[$cont] = $fields[$key];
            $cont++;
        }
        $result = $con->gotolayoutMultiFilter($this->layout,$keys,$vals);
        if (!$result) {
        	return false;
        } else {
        	$masters = array();
        	foreach ($result as $rec) {
        		foreach ($fields as $key => $value) {
        			$mast[$key] = $rec->getField($key); 
        		}
        		$master = new Master($mast["__pk"],$mast["d_firstName"],$mast["d_tipo"],$mast["d_precio"]);
        		array_push($masters, $master);
        	}
        	return $master;
        }
    }

    /**
     * Con la variable $con llama a la funcion insert de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function insert(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
        	$keys[$cont] = $key;
        	$values[$cont] = $values;
        	$cont++;
        }
        $result = $con->createNewRecord($this->layout,$parametros);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Con la variable $con llama a la funcion update de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @return array
     */
    function update(Connection $con){
        $parametros=get_object_vars($this);
        $fields=$parametros;
        $parametros=$con->clear($parametros);
        //$fm = $con->Connect();
        $cont = 0;
        foreach ($parametros as $key => $value) {
        	$keys[$cont] = $key;
        	$values[$cont] = $values;
        	$cont++;
        }
        unset($parametros["__pk"]);
        $result = $con->SetDatas($this->layout,"__pk",$fields["__pk"],$parametros);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Con la variable $con llama a la funcion delete de la clase Connection pasandole sus
     * debidos parametros y devuelve el resultado
     *
     * @param Connection $con
     * @param array
     */
    function delete(Connection $con){
        $parametros=get_object_vars($this);
        $result = $con->deleteMultiFilter($this->layout,"__pk",$parametros["__pk"]);
        if (!$result) {
        	return false;
        } else {
        	return true;
        }
    }
}