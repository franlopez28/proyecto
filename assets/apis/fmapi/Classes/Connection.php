<?php

require_once('FileMaker.php');
require_once('Config.php');

class Connection
{
    //Atributos
    private $host, $user, $pass, $dbname;
    private $fm;
    
    //Constructor
    public function __construct()
    {
        $this->host=Config::$HOST;
        $this->user=Config::$USER;
        $this->pass=Config::$PASS;
        $this->dbname=Config::$DBNAME;
    }

    /**
     * Inicia la Connection.
     *
     * @return Mensaje de error||Connection
     */
    public function Connect()
    {
        $this->fm = new Filemaker($this->dbname, $this->host, $this->user, $this->pass);
        $dbs = $this->fm->listDatabases();

        if (FileMaker::isError($dbs)) {
            return $dbs->getMessage();
        } else {
            return $this->fm;
        }
    }

    function newUUID()
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    function clear(array $parametros)
    {
        unset($parametros["layout"]);
        foreach ($parametros as $key=>$value){
            if (empty($value)){
                unset($parametros[$key]);
            }
        }
        return $parametros;
    }

    function gotolayout($FilemakerLayout, $FieldFilter, $RecordsFilters)
    {
        $fm = self::Connect();

        $findCommand = $fm->newFindCommand($FilemakerLayout);

        $findCommand->addFindCriterion($FieldFilter, $RecordsFilters);

        $result = $findCommand->execute();

        if (FileMaker::isError($result)) {
            $error = 'FileMaker Find Error  (' . $result->code . ')';
            return 0;

        }// end if

        $records = $result->getRecords();
        return $records;
    }

    function gotolayoutPags($FilemakerLayout, $FieldFilter, $RecordsFilters, $From, $Max)
    {
        $fm = self::Connect();

        $findCommand = $fm->newFindCommand($FilemakerLayout);

        $findCommand->addFindCriterion($FieldFilter, $RecordsFilters);
        $findCommand->setRange($From, $Max);

        $result = $findCommand->execute();

        if (FileMaker::isError($result)) {
            $error = 'FileMaker Find Error  (' . $result->code . ')';
            return 0;
        }// end if

        $records = $result->getRecords();
        return $records;
    }

    function gotolayoutByID($FilemakerLayout, $RecordId)
    {
        $fm = self::Connect();
        $records = $fm->getRecordById($FilemakerLayout, $RecordId);

        if (FileMaker::isError($result)) {
            echo $error = 'FileMaker Find Error  (' . $error->code . ')';
            //Exit Records
            exit();
        }// end if

        return $records;
    }

    function gotolayoutMultiFilter($FilemakerLayout, $FieldFilter, $RecordsFilters)
    {
        $fm = self::Connect();
        $cont = 0;
        $leng = count($FieldFilter);
        //session_start();
        //Connect to the database
        //Search on the view Filemaker

        $findCommand = $fm->newFindCommand($FilemakerLayout);

        //Filter Records

        while ($cont <= $leng) {
            if ($RecordsFilters[$cont]!=null&&$FieldFilter[$cont]!="layout") {
                $findCommand->addFindCriterion($FieldFilter[$cont], $RecordsFilters[$cont]);
            }
            //Save records in variables
            $cont = $cont + 1;
        }
        //Save records in variables
        $result = $findCommand->execute();


        //When dont Search records capture error
        if (FileMaker::isError($result)) {
            //echo $error = 'FileMaker Find Error  (' . $result->code . ')';

            return 0;
            //Exit Records
            // exit();
        }// end if
        $records = $result->getRecords();
        //echo  $records[0]->getField('d_finalName');

        return $records;
    }

    function gotolayoutMultiFilterPags($FilemakerLayout, $FieldFilter, $RecordsFilters, $From, $Max)
    {
        $fm = self::Connect();
        $cont = 0;
        $leng = count($FieldFilter);
        //session_start();
        //Connect to the database
        //Search on the view Filemaker

        $findCommand = $fm->newFindCommand($FilemakerLayout);

        //Filter Records

        while ($cont <= $leng) {
            $findCommand->addFindCriterion($FieldFilter[$cont], $RecordsFilters[$cont]);
            //Save records in variables
            $cont = $cont + 1;
        }

        $findCommand->setRange($From, $Max);
        //Save records in variables
        $result = $findCommand->execute();


        //When dont Search records capture error
        if (FileMaker::isError($result)) {
            echo $error = 'FileMaker Find Error  (' . $result->code . ')';

            return 0;
            //Exit Records
            exit();
        }// end if
        $records = $result->getRecords();
        //echo  $records[0]->getField('d_finalName');

        return $records;
    }

    function addRecord($FilemakerLayout)
    {
        $fm = self::Connect();
        $newPk = self::newUUID();
        $ArrayDatos['__pk'] = $newPk;

        $rec = $fm->createRecord($FilemakerLayout, $ArrayDatos);
        $result = $rec->commit();

        return $newPk;
    }

    function SetDatas($FilemakerLayout, $FieldFilter, $RecordsFilters, $Array)
    {
        $fm = self::Connect();
        $records = self::gotolayout($FilemakerLayout, $FieldFilter, $RecordsFilters);

        foreach ($records as $record) {
            //capture id Records
            $recordId = $record->getRecordId();
            foreach ($Array as $clave => $valor) {
                $rec = $fm->getRecordById($FilemakerLayout, $recordId);
                $rec->setField($clave, $valor);
                $result = $rec->commit();
            }//End Foreach

            $result = $rec->commit();
        }//End Foreach
        return $result;
    }

    function SetDatasById($FilemakerLayout, $RecordId, $Array)
    {
        $rec = self::gotolayoutByID($FilemakerLayout, $RecordId);
        foreach ($Array as $clave => $valor) {
            $rec->setField($clave, $valor);
        }//End Foreach
        $result = $rec->commit();
    }

    function createNewRecord($FilemakerLayout, $Array)
    {
        $newRecord = self::addRecord($FilemakerLayout);
        $res = self::SetDatas($FilemakerLayout, "__pk", $newRecord, $Array);
        if (FileMaker::isError($res)) {
            return 0;
        } else {
            return 1;
        }
    }

    function deleteMultiFilter($FilemakerLayout, $FieldFilter, $RecordsFilters)
    {
        $records = self::gotolayout($FilemakerLayout, $FieldFilter, $RecordsFilters);

        foreach ($records as $record) {
            $recordId = $record->getRecordId();
            $rec = $fm->getRecordById($FilemakerLayout, $recordId);
            $result = $rec->delete();
            if (FileMaker::isError($result)) {
                return 0;
            }
        }//End Foreach
        return 1;
    }

    function deleteRecord($FilemakerLayout, $RecordId)
    {
        $rec = self::gotolayoutByID($FilemakerLayout, $RecordId);
        $rec->delete();
    }

}
