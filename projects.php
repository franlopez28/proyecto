<?php
session_start();
if(!isset($_SESSION['loginuser'])){
	header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en" style="margin: 0px">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#333">
	<title>Feníe Energía</title>
	<meta name="description" content="Material Style Theme">
	<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">
	<link rel="stylesheet" href="assets/css/font.css">
	<link rel="stylesheet" href="assets/css/preload.min.css" />
	<link rel="stylesheet" href="assets/css/plugins.min.css" />
	<link rel="stylesheet" href="assets/css/style.green-500.min.css" />
	<link rel="stylesheet" href="assets/css/styles.css">
	<link rel="stylesheet" href="assets/css/reset-form.css" id="reset">
	<link rel="stylesheet" href="assets/css/cookies.css">
	<link rel="stylesheet" href="assets/css/stylesProjects.css">
	<link rel="stylesheet" href="assets/css/no-more-tablesProject.css">
	<link rel="stylesheet" href="assets/css/tabs.css">
	<link rel="stylesheet" href="assets/css/tabla.css">
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="assets/js/jquery-3.2.1.min.js" type="application/javascript"></script>
	<script src="assets/js/projects.js"></script>

</head>
<body style="background-image: url(assets/img/login.jpeg); background-size: cover; background-repeat: no-repeat; overflow: none;">
	<nav class="navbar ms-navbar-primary" style="background-color: rgba(255, 255, 255, 0.43); margin: 0px; box-shadow: 0 0 30px 15px rgba(156, 156, 156, 0.44)">
		<div class="container container-full">
			<div class="ms-title">
				<a href="" style="float:left">
					<img src="assets/img/logo.png" alt="Brim" style="width:200px;margin-bottom: 5%;margin-top: 5%;float: left;margin-left: 10%; ">  
				</a>
			</div>
			<div style="float: right; color: green;">
				<a href="logout.php" data-toggle="tooltip" data-placement="left" title="" data-original-title="Logout" style="position: absolute; top: 0px; right: 0px; background-color: rgb(32, 146, 37)">
					<span class="ms-icon ms-icon-xlg ms-icon-square spanArrow" style="width: 50px;height: 55px;box-shadow: 0.19em 0.19em 0.68em 0em rgba(0,0,0,0.25);float:left; margin: 0px;">
						<i class="fa fa-power-off" style="padding-top: 0px;padding-left: 0px;font-weight: bold;font-size: 26px;"></i>
					</span>
				</a>
			</div>  
		</div>
	</nav>
	<div id="ms-preload" class="ms-preload">
		<div id="status">
			<div class="spinner">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
	</div>
	<div class="wrap bg-warning1">
		<div class="tab">
			<button class="tablinks" onclick="openTabs(event, 'Datos')" id="defaultOpen">Datos</button>
			<button class="tablinks" onclick="openTabs(event, 'Facturas')">Facturas</button>
			<button class="tablinks" onclick="openTabs(event, 'Consumo')">Consumo</button>
		</div>
		<div id="Datos" class="tabcontent">
			<h4>Datos Personales</h4>
			<label style="color: black">Nombre</label> 
			<input type="text" id="nombre" />
			<label style="color: black">Apellidos</label> 
			<input type="text" id="apellido"/>
			<label style="color: black">Email</label> 
			<input type="email" id="email"/>
			<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">Cambiar contraseña</h4>
						</div>
						<div class="modal-body">
							<label style="color: black">Contraseña Actual</label> 
							<input type="text" id="contrasena" disabled />
							<label style="color: black">Contraseña nueva</label> 
							<input type="text" id="contrasenanueva"  onchange="validatePass()" />
							<label style="color: black">Confirmar contraseña</label> 
							<input type="text" id="contrasenaconfirmar"  onchange="validatePass()" />
							<button id="btnChPass" type="button" data-toggle="modal" data-target="#miModal" class="botonRightModal" disabled onclick="changepassword()">Guardar</button>
							<button type="button" data-toggle="modal" data-target="#miModal" class="botonLeftModal">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
			
			<h4>Datos de facturación</h4>
			<label style="color: black">Dirección</label> 
			<input type="text" id="address" />
			<label style="color: black">Código postal</label> 
			<input type="text" id="pc"/>
			<label style="color: black">Poblacion</label> 
			<input type="text" id="town"/>
			<label style="color: black">Provincia</label> 
			<input type="text" id="province"/>
			<label style="color: black">Pais</label> 
			<input type="text" id="country"/>
			<div style="float: left;padding-top: 10px;">
				<button type="button" data-toggle="modal" data-target="#miModal" class="botonLeft">Cambiar contraseña</button>
				<!--<button class="botonLeft2" data-toggle="modal" data-target="#miModal2">Cambiar Contrato</button>-->
				<button id="guardar" onclick="saveUserData()" class="botonRight">Guardar</button>
			</div>
		</div>
		<div id="Facturas" class="tabcontent">
			<h4>Facturas</h4>
			<div id="mainselection">
				<select onchange="loadContratos(this.value)" id="filteryear"></select>
			</div>
			
			<table id="tablacontratos">
				<thead>
					<tr  class="header">
					<th style="width:3%;">Mes</th>
					<th style="width:20%;">Fecha</th>
					<th style="width:20%;">Total</th>
					<th style="width:5%;">Opciones</th>
				</tr>
				</thead>
				<tbody id="contratoslist"></tbody>
			</table> 
		</div>
		<div id="Consumo" class="tabcontent">
			<h4>Consumo</h4>
			<div id="mainselection">
				<select onchange="loadContratos(this.value)" id="filteryearGrafica"></select>
			</div>
			  
  				<div id="chart_div" style="margin-top: -10em; "></div>
      
		</div>
	
	</div>

	<div class="btn-back-top">
		<a href="#" data-scroll id="back-top" style="background-color: rgb(32, 146, 37);" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
			<i class="zmdi zmdi-long-arrow-up"></i>
		</a>
	</div>
	
	
	<script src="assets/js/plugins.min.js"></script>
	<script src="assets/js/app.min.js"></script>
</body>
</html>