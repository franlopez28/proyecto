<?php
session_start();
if(isset($_SESSION['loginuser'])){
	header('Location: projects.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#333">
	<title>Feníe Energía</title>
	<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">
	<link rel="stylesheet" href="assets/css/font.css">
	<link rel="stylesheet" href="assets/css/preload.min.css">
	<link rel="stylesheet" href="assets/css/plugins.min.css">
	<link rel="stylesheet" href="assets/css/style.green-500.min.css">
	<link rel="stylesheet" href="assets/css/stylesProjects.css">
	<script src="assets/js/jquery-3.2.1.min.js" type="application/javascript"></script>
	<script src="assets/js/index.js"></script>
</head>
<body>
	<div id="ms-preload" class="ms-preload" style="display: none;">
		<div id="status" style="display: none;">
			<div class="spinner">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
	</div>
	<div class="bg-full-page bg-primary back-fixed" style="background-image: url(assets/img/login.jpeg); background-size: cover; background-repeat: no-repeat; overflow: none;">
		<div class="mw-500 absolute-center">
			<div class="card" style="background-color: rgba(0, 0, 0, 0.32) !important">
				<div class="card-block">
					<h1 style="text-align: center;">Iniciar sesión</h1>
					<form class="login" id="formlogin">
						<fieldset>
							<div class="form-group label-floating is-empty">
								<div class="input-group">
									<span class="input-group-addon">
									<i class="zmdi zmdi-account" style="color: white"></i>
									</span>
									<label class="control-label" for="ms-form-user" style="color: white !important">Usuario</label>
									<input type="text" name="email" id="email" class="form-control" style="color: white;"> 
								</div>
								<span class="material-input"></span>
							</div>
							<div class="form-group label-floating is-empty">
								<div class="input-group">
									<span class="input-group-addon">
									<i class="zmdi zmdi-lock" style="color: white"></i>
									</span>
									<label class="control-label" for="ms-form-pass" style="color: white !important">Contraseña</label>
									<input type="password" name="passwd" id="passwd" class="form-control" style="color: white !important">
								</div>
								<span class="material-input"></span>
							</div>
							<div class="row">
								<div class="col-md-12">
									<button type="button" onclick="login()" class="btn btn-raised btn-primary btn-block">Entrar
									<i class="zmdi zmdi-long-arrow-right no-mr ml-1"></i>
									</button>
								</div>
							</div>
						</fieldset>
					</form>
					<div id="errorMessage" style="color: white;text-align: center;background:red ; font-weight: bold;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="assets/js/plugins.min.js"></script>
	<script src="assets/js/app.min.js"></script>
</body>
</html>