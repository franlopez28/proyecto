<!DOCTYPE html>
<html lang="en" style="margin: 0px">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Feníe Energía</title>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="assets/js/jquery-3.2.1.min.js" type="application/javascript"></script>
	<script type="text/javascript">
		var fecha = new Date();
		var year = fecha.getFullYear();
		var datos = [];

		document.addEventListener('DOMContentLoaded', function() {
			loadContratos(<?php echo $_REQUEST['year'];?>);
		});

		function loadContratos(anyo)
		{
			year = anyo;
			var parametros = "year="+year;
			$.ajax({
				method: "POST",
				url: "assets/apis/fmapi/Services/loadContratos.php",
				data: parametros,
				dataType: "json",
				beforeSend: function (){
				},
				success: function (response) {
					if (response.success=="1") {
						clearDraw();
						
						for (var i = 0; i < response.data.length; i++) {
							var monthnum = parseInt(response.data[i].l_mes);
							
							var amount = response.data[i].l_TotalPlusVAT;
							var item = parseInt(amount.replace(",","."));
							datos[monthnum - 1][1]=item;
						}

						drawBasic();
					}else{
						clearDraw();
						drawBasic();
					}
				}
			});
		}
		function clearDraw(){
			datos[0]=["Enero",null] ;
			datos[1]=["Febrero",null] ;
			datos[2]=["Marzo",null] ;
			datos[3]=["Abril",null] ;
			datos[4]=["Mayo",null] ;
			datos[5]=["Junio",null] ;
			datos[6]=["Julio",null] ;
			datos[7]=["Agosto",null] ;
			datos[8]=["Septiembre",null] ;
			datos[9]=["Octubre",null] ;
			datos[10]=["Noviembre",null] ;
			datos[11]=["Diciembre",null] ;
		}

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Meses');
			data.addColumn('number', 'Consumo');
			data.addRows(datos);

			var options = {
			title: 'AÑO '+year,
			width: $(window).width()*1,
			height: $(window).height()*1.3,
			colors: ['#4caf50'],
			legend: 'none',
			bar: {groupWidth: '85%'},
			chartArea: {
			backgroundColor: {
			fill: '#ccc',
			fillOpacity: 0.3
			},

			},
			backgroundColor: {
			fill: '#fff',
			fillOpacity: 0.1
			},
			vAxis: { gridlines: { count: 4 } }
			};

			var chart = new google.visualization.BarChart(
			document.getElementById('chart_div'));

			chart.draw(data, options);
		}
	</script>
</head>
<body>
	<div id="chart_div" style="margin-top: -6em;"></div>
	<script src="assets/js/plugins.min.js"></script>
	<script src="assets/js/app.min.js"></script>
</body>
</html>